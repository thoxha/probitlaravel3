<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('is_admin');
    }

    public function dashboard()
    {

        return view('admin.dashboard');
    }

    public function articlesIndex()
    {
        $articles = Article::orderBy('created_at', 'desc')->withTrashed()->paginate(10);
        return view('admin.articles_index', compact('articles'));
    }

    public function articlesDelete(Request $request)
    {
        $article_ids = $request->input('delete_article');
        $metoda = $request->input('metoda');
        if ($article_ids == null) {
            return redirect(route('admin.dashboard'));
        } else {
            $raporti = "";
            foreach ($article_ids as $id => $value) {
                if ($value == "on") {
                    $article = Article::withTrashed()->find($id);
                    if ($article !== null) {
                        $raporti .= "<p>U fshi artikulli: " . $article->title . "</p>";
                        if ($metoda == "soft") {
                            $article->delete();
                        } else {
                            $article->forceDelete();
                        }
                    }
                }
            }
        }

        return view('report', ['report' => "<p>Artikuj u fshine me sukses</p>" . $raporti]);
    }

    public function articlesRestore(Request $request)
    {
        $article_ids = $request->input('restore_article');
        if ($article_ids == null) {
            return redirect(route('admin.dashboard'));
        } else {
            $raporti = "";
            foreach ($article_ids as $id => $value) {
                if ($value == "on") {
                    $article = Article::withTrashed()->find($id);
                    if ($article !== null) {
                        $raporti .= "<p>U kthye artikulli: " . $article->title . "</p>";
                        $article->restore();
                    }
                }
            }
        }

        return view('report', ['report' => "<p>Artikuj u fshine me sukses</p>" . $raporti]);
    }


    public function articleForm($id = 0)
    {
        if ($id > 0) {
            $article = Article::findOrFail($id);
        } else {
            $article = (object)[
                'id' => 0,
                'title' => "",
                'slug' => "",
                'content' => "",
                'user_id' => Auth::id(),
                'category_id' => 0
            ];
        }


        $categories = Category::orderBy('category_name')->get();
        return view('articles.article_form', compact('categories', 'article'));
    }


    public function submit(Request $request)
    {
        $article_id = (int)$request->input('article_id');


        if ($request->has('fshirja')) {
            $article = Article::find($article_id);
            Storage::disk('products')->delete(substr($article->photo, 8, strlen($article->photo)));
            $article->delete();
            return view('report', ['report' => "Artikulli u fshi me sukses!"]);

        } else {


            if ($request->input('article_id') == 0) {
                $request->validate(
                    [
                        'title' => 'required|string|min:5|max:255',
                        'content' => 'required|min:100',
                        'category_id' => 'required|exists:categories,id|integer',
                        'photo' => 'nullable|file|image|mimes:jpeg,png'

                    ]
                );
            } else {
                $request->validate(
                    [
                        'article_id' => 'required|exists:articles,id',
                        'title' => 'required|string|min:5|max:255',
                        'content' => 'required|min:100',
                        'category_id' => 'required|exists:categories,id|integer',
                        'photo' => 'nullable|file|image|mimes:jpeg,png'
                    ]
                );
            }


            $title = filter_var($request->input('title'), FILTER_SANITIZE_STRING);
            $content = filter_var($request->input('content'), FILTER_SANITIZE_STRING);
            $category_id = filter_var($request->input('category_id'), FILTER_SANITIZE_STRING);

            if ($article_id == 0) {
                $article = new Article();
            } else {
                $article = Article::find($article_id);
            }

            $article->title = $title;
            $article->content = $content;
            $article->user_id = Auth::id();
            $article->category_id = $category_id;

            if ($request->has('photo')) {
                $path = $request->file('photo')->store('products');
                $article->photo = $path;
            }


            $article->save();

            return view('report', ['report' => "Artikulli " . $article->id . " u shtua/perditesua!"]);
        }
    }
}
