<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ArticleController extends Controller
{

    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate(12);
        return view('articles.index', compact('articles'));
    }


    public function show(Article $article)
    {
        return view('articles.article', compact('article'));
    }


    public function editArticle($id)
    {
        $article = Article::findOrFail($id);
        /*        if (Gate::allows('edit-article', $article)) {
                    $categories = Category::orderBy('category_name')->get();
                    return view('articles.article_form', compact('categories', 'article'));
                } else {
                    return view('report', ['report' => "Nuk keni te drejte t'i qaseni ketij artikulli"]);
                }*/

        $category = 1;
        Gate::authorize('edit-article', [$article, $category]);
        $categories = Category::orderBy('category_name')->get();
        return view('articles.article_form', compact('categories', 'article'));

    }






    public function category(Category $category)
    {
        return view('articles.articles_by_category', compact('category'));
    }


    public function byCategory(Category $category) {
       $articles = Article::where('category_id', $category->id)->get();
       return view('articles.by_category', compact('articles'));
    }


    public function author(User $user)
    {
        return view('articles.articles_by_author', compact('user'));
    }


    public function commentsAdd(Request $request)
    {
        $request->validate(
            [
                'comment_text' => 'required|min:10'
            ]
        );

        $comment_text = filter_var($request->input('comment_text'), FILTER_SANITIZE_STRING);

        $article_id = (int)$request->input('article_id');
        $comment = new Comment();
        $comment->comment_text = $comment_text;
        $comment->user_id = Auth::id();
        $comment->article_id = $article_id;
        $comment->save();
        return redirect(route('articles.show', [$comment->article]));
    }

    public function search(Request $request) {
       $search = filter_var($request->input('search'), FILTER_SANITIZE_STRING);
       $articles = Article::where('title', 'like', '%'.$search."%")
           ->orWhere('content', 'like', '%'.$search.'%')->orderBy('created_at', 'desc')
           ->paginate(12);
       return view('articles.search', compact('articles', 'search'));
    }




}
