<?php

namespace App\Http\ViewComposers;

use App\Article;
use App\Category;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class BaseViewComposer
{


    public function compose(View $view)
    {
        $data = $view->getData();

//        $data['main_categories'] = Category::orderBy('category_name')->get();

        $data['main_categories'] = Cache::get('main_categories', function () {
            $categories = Category::orderBy('category_name')->get();
            Cache::put('main_categories', $categories, 600);
            return $categories;
        });

        $data['articles_count'] = Article::count();
        $data['viti_aktual'] = date("Y");
        $view->with($data);
    }
}
