<?php

namespace App;

use App\Scopes\AgeScope;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getRouteKeyName()
    {
        return 'username';
    }

    public function getAdminAttribute()
    {
        return $this->attributes['level'] == 5;
    }

    public function getModeratorAttribute()
    {
        return $this->attributes['level'] == 4;
    }

    public function articles() {
        return $this->hasMany('\App\Article');
    }

    public function comments() {
        return $this->hasMany('\App\Comment');
    }



}
