<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   public $timestamps = false;

   public function getRouteKeyName()
   {
       return 'category_slug';
   }

    public function articles() {
       return $this->hasMany('\App\Article');
   }
}
