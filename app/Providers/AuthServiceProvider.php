<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('edit-article', function ($user, $article, $category) {
//            $days_between = ceil(abs(strtotime(date("Y-m-d")) - strtotime(substr($article->created_at, 0, 10))) / 86400);

            return ($user->id === $article->user_id && $article->category_id == $category)
                || $user->level == 4 ||  $user->level == 5;

        });



    }
}
