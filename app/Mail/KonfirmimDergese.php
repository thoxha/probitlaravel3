<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class KonfirmimDergese extends Mailable
{
    use Queueable, SerializesModels;

    protected $porosia;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($porosia)
    {
        $this->porosia = $porosia;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
//            ->attach('C:\Users\alkim\Google Drive\Web Design libri\pjesa001.pdf')
            ->view('emails.porosia')

            ->text('emails.porosia_text')
            ->with([
                'emri' => $this->porosia->emri,
                'vlera' => $this->porosia->vlera,
            ]);
    }
}
