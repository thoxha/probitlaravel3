<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\HomeController;
use App\Mail\KonfirmimDergese;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('articles')->group(function() {
    Route::get('index', [ArticleController::class, 'index'])->name('articles.index');
    Route::get('show/{article}', [ArticleController::class, 'show'])->name('articles.show');
    Route::get('category/{category}', [ArticleController::class, 'category'])->name('articles.category');
    Route::get('author/{user}', [ArticleController::class, 'author'])->name('articles.author');
    Route::get('by_category/{category}', [ArticleController::class, 'byCategory'])->name('articles.by_category');
    Route::get('search', [ArticleController::class, 'search'])->name('articles.search');
});

Route::prefix('admin')->middleware('is_admin')->group(function() {
    Route::get('dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('article_form/{article?}', [AdminController::class, 'articleForm'])->name('admin.articles.article_form');
    Route::post('submit', [AdminController::class, 'submit'])->name('admin.articles.submit');
    Route::get('articles_index', [AdminController::class, 'articlesIndex'])->name('admin.articles.articles_index');
    Route::post('articles_delete', [AdminController::class, 'articlesDelete'])->name('admin.articles.articles_delete');
    Route::post('articles_restore', [AdminController::class, 'articlesRestore'])->name('admin.articles.articles_restore');
});
