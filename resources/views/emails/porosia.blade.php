<h1 style="color:blue">Konfirmimi i porosise</h1>

<p style="color: purple;">Pershendetje, {{ $emri }}</p>
<p style="color: purple;">Me kete email konfirmojme se kemi pranuar porosine tuaj.</p>
<div>
    <img src="{{ $message->embed('C:\Users\alkim\Google Drive\Web Design libri\doctype.png') }}">
</div>
<p style="color: purple;">Vlera e porosise: <span style="color:darkcyan; font-weight: bold">{{ $vlera }}</span></p>
