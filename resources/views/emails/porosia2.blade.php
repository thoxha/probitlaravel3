<h1 style="color:blue">Konfirmimi i porosise</h1>

<p style="color: purple;">Pershendetje, {{ $porosia->emri }}</p>
<p style="color: purple;">Me kete email konfirmojme se kemi pranuar porosine tuaj.</p>

<p style="color: purple;">Vlera e porosise: <span style="color:darkcyan; font-weight: bold">{{ $porosia->vlera }}</span></p>
