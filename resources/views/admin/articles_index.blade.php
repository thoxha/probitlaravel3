@extends('layouts.app')

@section('content')
    <h1>Lajmet</h1>

    <form method="post" action="{{ route('admin.articles.articles_delete') }}">
        @csrf
        <table class="table">
            <tr>
                <th>Kthe</th>
                <th>Lajmi</th>
                <th>Fshij</th>
            </tr>
            @foreach($articles as $article)
                <tr>
                    <td><input type="checkbox" name="restore_article[{{ $article->id }}]"></td>
                    <td><a href="{{ route('admin.articles.article_form', [$article->id]) }}">{{ $article->title }}</a></td>
                    <td><input type="checkbox" name="delete_article[{{ $article->id }}]"></td>

                    <td>
                        @if ($article->deleted_at != null)
                            E fshire
                        @endif
                    </td>
                </tr>
            @endforeach
            <tr>
                <td><input type="submit" class="btn btn-success" value="RESTORE" formaction="{{ route('admin.articles.articles_restore') }}">
                </td>
                <td></td>
                <td>
                    <div>
                        <select name="metoda">
                            <option value="soft">SOFT DELETE</option>
                            <option value="force">FORCE DELETE</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-warning" value="DELETE">
                </td>
                <td></td>
            </tr>
        </table>

    </form>
    <div>
        {{ $articles->links() }}
    </div>
@endsection
