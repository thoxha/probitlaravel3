@extends('layouts.app')

@section('content')
    <div class="row">
        @if ($articles->count() > 0)
        <table class="table">
            @foreach($articles as $article)
                <tr>
                    <td>{{ date('d.m.Y H:i:s', strtotime($article->created_at)) }}</td>
                    <td><h4><a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></h4></td>
                </tr>
            @endforeach
        </table>
        <div>
            {{ $articles->appends(['search' => $search])->links() }}
        </div>
        @else
            <p>Nuk ka lajme me kete kriter!</p>
        @endif
    </div>
@endsection

@section('sidebar')
    <h1>Sidebar</h1>
@endsection
