@extends('layouts.app')

@section('content')
    @if ($user != null)
        @foreach($user->articles as $article)
            <p>{{ $article->user->name }} <a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></p>
        @endforeach
    @endif
@endsection
