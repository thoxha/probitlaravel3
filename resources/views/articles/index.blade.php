@extends('layouts.app')

@section('content')
    <div class="row">
        @foreach($articles as $article)
            <div class="col-md-4">
                <h4>{{ $article->title }}</h4>
                <div>
                    {{ mb_substr($article->content, 0, 100) }}...
                </div>
                <div>
                    <a href="{{ route('articles.show', [$article]) }}">Më tepër</a>
                </div>
            </div>
        @endforeach
            <div>
                {{ $articles->links() }}
            </div>
    </div>
@endsection

@section('sidebar')
    <h1>Sidebar</h1>
@endsection
