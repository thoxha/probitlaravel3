@extends('layouts.app')

@section('content')
    @if ($category != null)
        @foreach($category->articles as $article)
            <p>{{ $article->category->category_name }} <a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></p>
        @endforeach
    @endif
@endsection
