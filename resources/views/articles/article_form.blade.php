@extends('layouts.app')

@section('content')
    <h1>Artikull i ri</h1>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form method="POST" action="{{ route('admin.articles.submit') }}" enctype="multipart/form-data">
        @csrf
        <label for="title">Titulli</label>
        <input type="hidden" name="article_id" value="{{ $article->id }}">
        <input type="text" name="title" id="title" class="form-control" value="{{ old('title', $article->title) }}">

        <label for="content">Lajmi</label>
        <textarea name="content" id="content" rows="10" class="form-control">{{ old('content', $article->content) }}</textarea>

        <label for="photo">Fotografia</label>
        <input type="file" name="photo" id="photo">

        <div>
            <label for="category_id">Kategoria</label>
            <select name="category_id" id="category_id" class="form-control">
                @foreach($categories as $category)
                    <option @if($category->id == $article->category_id) selected @endif value="{{ $category->id }}">{{ $category->category_name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <input type="checkbox" name="fshirja">FSHIJE
        </div>
        <div>
            <input type="submit" class="btn btn-primary" value="DERGO">
        </div>
    </form>
@endsection
