@extends('layouts.app')

@section('content')
    @if (count($articles) > 0)
        @foreach($articles as $article)
            <p>{{ $article->category->category_name }} <a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></p>
        @endforeach
    @else
        <p>Nuk ka asnje lajm per kete rubrike!</p>
    @endif
@endsection
