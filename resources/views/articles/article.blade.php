@extends('layouts.app')




@section('content')
    <h1>{{ $article->title }}
        @if (Auth::check() && Auth::user()->admin)
            <a class="btn btn-primary" href="{{ route('admin.articles.article_form', [$article->id]) }}">Edito</a>
        @endif
    </h1>
    @if (!empty($article->photo))
        <img src="{{ env('APP_URL')."/".$article->photo }}" class="img-fluid">
    @endif
    <blockquote>{{ $article->content }}</blockquote>
    <p>Rubrika: <a
            href="{{ route('articles.category', [$article->category]) }}">{{ $article->category->category_name }}</a>
    </p>
    <p>Autori: <a href="{{ route('articles.author', [$article->user]) }}">{{ $article->user->name }}</a></p>

    {{--        @if (Auth::check())--}}
    {{--            <form method="POST" action="{{ route('comments.add') }}">--}}
    {{--                @csrf--}}
    {{--                <input type="hidden" name="article_id" value="{{ $article->id }}">--}}
    {{--                <textarea class="form-control" rows="10" name="comment_text"--}}
    {{--                          placeholder="Sheno komentin">{{ old('comment_text') }}</textarea>--}}
    {{--                <input type="submit" value="DERGOJE" class="btn btn-primary">--}}
    {{--            </form>--}}
    {{--        @endif--}}

    @foreach($article->comments as $comment)
        <div>
            <p>{{ $comment->user->name }}</p>
            <blockquote>{{ $comment->comment_text }}</blockquote>
            <p>{{ $comment->created_at }}</p>
        </div>
    @endforeach

@endsection


@section('sidebar')
    <h1>Sidebar</h1>
@endsection


